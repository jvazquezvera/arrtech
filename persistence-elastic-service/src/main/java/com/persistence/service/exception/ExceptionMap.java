package com.persistence.service.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.log4j.Logger;

import com.persistence.service.dto.GenericResponse;

/**
 *  Descripción:
 * 
 * @author jvazquez [Javier Vázquez Vera]
 * ExceptionMap.java
 */

@Provider
public class ExceptionMap implements ExceptionMapper<WebApplicationException>{

	private static final Logger LOGGER = Logger.getLogger(ExceptionMap.class);

	@Override
	public Response toResponse(WebApplicationException exception) {
		LOGGER.info(":::"+this.getClass());
		LOGGER.info("status=start, ExceptionMap");
		
		GenericResponse respons = new GenericResponse();
		respons.setRespuesta("ERROR");
		respons.setDescripcion(exception.getCause().getMessage());
		
		LOGGER.info("status=end, ExceptionMap");
		return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(respons).build();
	}
}
