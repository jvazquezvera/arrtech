/**
 * Copyright (c) 2017
 * administrar-orden-mc - ExecutionException.java
 * May 2, 2017
 */
package com.persistence.service.exception;

/**
 *  Descripción: Esta clase es usada para el manejo de excepciones
 * 
* @author jvazquez [Javier Vázquez Vera]
 * ExceptionMap.java
 */
public class ExecutionException extends Exception {


	private static final long serialVersionUID = 8954748990951293839L;

	public ExecutionException() {
	}

	public ExecutionException(String message) {
		super(message);
	}

	public ExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExecutionException(Throwable cause) {
		super(cause);
	}

	public ExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
