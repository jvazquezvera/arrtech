package com.persistence.service.processor;



import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.persistence.service.dto.CfdiElastic;
import com.persistence.service.dto.CfdiSearch;
import com.persistence.service.elastic.beans.QueryEls;
import com.persistence.service.elastic.util.Util;

public class ElasticSearchProcesor implements Processor {

	
	
	@Override
	public void process(Exchange exchange) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		CfdiSearch cfdi =(CfdiSearch) exchange.getIn().getBody();
		QueryEls els= Util.toQueryEls(cfdi);
		
		String jsonInString;
		try {
			jsonInString = mapper.writeValueAsString(els);
			System.out.println("salida from pro: "+jsonInString);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		exchange.getIn().setBody(els);
				
	}

	

   
	
}
