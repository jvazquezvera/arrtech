package com.persistence.service.processor;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.persistence.service.dto.CfdiElastic;

public class ElasticInsertProcesor implements Processor {

	private static final String RFC_EMISOR="rfcReceptor";
	private static final String RFC_RECEPTOR="rfcEmisor";
	private static final String FECHA_EMISION="fechaEmision";
	private static final String FECHA_TIMBRADO="fechaTimbrado";
	private static final String UUID="uuid";
	private static final String XML_AS_STRING="xml_as_string";
	
	
	
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		CfdiElastic saveCfdi= new CfdiElastic();
		saveCfdi.setFechaEmision(exchange.getIn().getHeader(FECHA_EMISION).toString());
		saveCfdi.setFechaTimbrado(exchange.getIn().getHeader(FECHA_TIMBRADO).toString());
		saveCfdi.setRfcEmisor(exchange.getIn().getHeader(RFC_EMISOR).toString());
		saveCfdi.setRfcReceptor(exchange.getIn().getHeader(RFC_RECEPTOR).toString());
		saveCfdi.setUuid(exchange.getIn().getHeader(UUID).toString());
		Document doc = convertStringToDocument(exchange.getIn().getHeader(XML_AS_STRING).toString());
	    String str = convertDocumentToString(doc);
		saveCfdi.setCfdi(str);
		exchange.getIn().setHeader(XML_AS_STRING, "");
		exchange.getIn().setBody(saveCfdi);
		
	}

	

    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        
        return null;
    }
	
    
    private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder;  
        try  
        {  
            builder = factory.newDocumentBuilder();  
            Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) ); 
            return doc;
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }

	
}
