
package com.persistence.service.processor;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXB;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import com.persistence.service.dto.GenericResponse;

/**
 *  Descripcion:
 * 
 * @author jvazquez [Javier V. Vazquez Vera]
 * ResponseBean.java
 */
public class ResponseBean  {

	private static final Logger LOGGER = Logger.getLogger(ResponseBean.class);

	public void errorResponse(Exchange exchange) throws IOException {

		LOGGER.info("persistence-elastic-service, status=start, ResponseBean:::errorResponse ");

		String error = exchange.getIn().getBody(String.class);

		GenericResponse respons = new GenericResponse();
		respons.setRespuesta("ERROR");
		respons.setDescripcion(error);

		LOGGER.info("persistence-elastic-service, status=end, ResponseBean:::errorResponse ");
		exchange.getOut().setBody(respons);
	}


	public void outResponse(Exchange exchange) throws IOException {

		LOGGER.info("persistence-elastic-service, status=start, ResponseBean:::outResponse ");

		String respuesta = exchange.getIn().getBody(String.class);

		GenericResponse answer= null;

		LOGGER.info(respuesta);

		answer = JAXB.unmarshal(new StringReader(respuesta), GenericResponse.class);	

		exchange.getOut().setBody(answer);			

		LOGGER.info("persistence-elastic-service, status=end, ResponseBean:::outResponse ");
	}


}
