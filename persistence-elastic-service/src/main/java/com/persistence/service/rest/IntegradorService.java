/**
 * Copyright (c) 2017
 * integrador-bursa-service - IntegradorService.java
 * Sep 4, 2017
 */
package com.persistence.service.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.persistence.service.dto.CfdiElastic;
import com.persistence.service.dto.CfdiSearch;


/**
 *  Descripción: Defición de servicio rest Integrador
 * 
 * @author avazquez [Javier V. Vazquez Vera]
 * IntegradorService.java
 */

@Path("/")
public interface IntegradorService {

	
	
	@POST
	@Path(value = "/saveCfdi")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveCfdi(String request);
	
	@POST
	@Path(value = "/consultaCfdi")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultaCfdi(CfdiSearch request);
	
		

}
