package com.persistence.service.elastic.util;

import java.util.HashMap;
import java.util.Map;

import com.persistence.service.dto.CfdiElastic;
import com.persistence.service.dto.CfdiSearch;
import com.persistence.service.elastic.beans.ElementQueryEls;
import com.persistence.service.elastic.beans.QueryEls;
import com.persistence.service.elastic.beans.QueryString;

public class Util {

	
	public static QueryEls toQueryEls(CfdiSearch cfdi) {
		
		QueryEls els= new QueryEls();
		Map<String,ElementQueryEls> mapa= new HashMap<>();
		QueryString qry= new QueryString();
		
		StringBuffer query= new StringBuffer();
		
		
			query.append("rfcEmisor:"+cfdi.getRfcEmisor()+"*");
			query.append(" AND rfcReceptor:"+cfdi.getRfcReceptor()+"*");
			query.append(" AND uuid:"+cfdi.getUuid()+"*");
			query.append(" AND fechaEmision:["+(cfdi.getFechaEmisionInicial().isEmpty()?"*":cfdi.getFechaEmisionInicial())+" TO "+(cfdi.getFechaEmisionFinal().isEmpty()?"*":cfdi.getFechaEmisionFinal())+"]");
			query.append(" AND fechaTimbrado:["+(cfdi.getFechaTimbradoInicial().isEmpty()?"*":cfdi.getFechaTimbradoInicial() )+" TO "+(cfdi.getFechaTimbradoFinal().isEmpty()?"*":cfdi.getFechaTimbradoFinal())+"]");
		
		
		qry.setQuery(query);
		mapa.put("query_string", qry );
		els.setQuery(mapa);
		
		return els;
		
	}
	
}
