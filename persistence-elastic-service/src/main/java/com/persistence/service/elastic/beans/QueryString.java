package com.persistence.service.elastic.beans;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value="QueryString")
public class QueryString implements ElementQueryEls{

	private Object query;

	public Object getQuery() {
		return query;
	}

	public void setQuery(Object query) {
		this.query = query;
	}
	
	
	
	
	
}
