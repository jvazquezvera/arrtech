package com.persistence.service.elastic.beans;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value="QueryEls")
public class QueryEls {

	
	
	Map<String, ElementQueryEls> query;

	public Map<String, ElementQueryEls> getQuery() {
		return query;
	}

	public void setQuery(Map<String, ElementQueryEls> query) {
		this.query = query;
	}

	
	
}
