package com.persistence.service.dto;

import com.fasterxml.jackson.annotation.JsonRootName;


@JsonRootName(value="CFDI")
public class CfdiElastic {
	
	private String rfcEmisor="";
	private String rfcReceptor="";
	private String fechaEmision="";
	private String fechaTimbrado="";
	private String uuid="";
	private String cfdi="";
	
	public String getRfcEmisor() {
		return rfcEmisor;
	}
	public void setRfcEmisor(String rfcEmisor) {
		this.rfcEmisor = rfcEmisor;
	}
	public String getRfcReceptor() {
		return rfcReceptor;
	}
	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaTimbrado() {
		return fechaTimbrado;
	}
	public void setFechaTimbrado(String fechaTimbrado) {
		this.fechaTimbrado = fechaTimbrado;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getCfdi() {
		return cfdi;
	}
	public void setCfdi(String cfdi) {
		this.cfdi = cfdi;
	}
	

	
}
