package com.persistence.service.dto;

public class CfdiSearch extends CfdiElastic {

	
	private String fechaEmisionInicial="";
	private String fechaEmisionFinal="";
	private String fechaTimbradoInicial="";
	private String fechaTimbradoFinal="";
	
	
	public String getFechaEmisionInicial() {
		return fechaEmisionInicial;
	}
	public void setFechaEmisionInicial(String fechaEmisionInicial) {
		this.fechaEmisionInicial = fechaEmisionInicial;
	}
	public String getFechaEmisionFinal() {
		return fechaEmisionFinal;
	}
	public void setFechaEmisionFinal(String fechaEmisionFinal) {
		this.fechaEmisionFinal = fechaEmisionFinal;
	}
	public String getFechaTimbradoInicial() {
		return fechaTimbradoInicial;
	}
	public void setFechaTimbradoInicial(String fechaTimbradoInicial) {
		this.fechaTimbradoInicial = fechaTimbradoInicial;
	}
	public String getFechaTimbradoFinal() {
		return fechaTimbradoFinal;
	}
	public void setFechaTimbradoFinal(String fechaTimbradoFinal) {
		this.fechaTimbradoFinal = fechaTimbradoFinal;
	}
	

	
	
}