package com.persistence.service.dto;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 *  Descripción:
 * 
 * @author jvazquez [Javier Valentin Vázquez Vera]
 * GenericResponse.java
 */

@JsonInclude(Include.NON_NULL)
@JsonRootName(value="Response")
public class GenericResponse {

    private String respuesta;
    private String descripcion;

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
    
    
}
