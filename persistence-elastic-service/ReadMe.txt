Camel Project for Blueprint 
=========================================

Install FAbric
fabric:profile-edit --feature camel-core bmtk-base-profile
fabric:profile-edit --feature camel-blueprint bmtk-base-profile
fabric:profile-edit --feature camel-cxf bmtk-base-profile
fabric:profile-edit --feature camel-jackson bmtk-base-profile
fabric:profile-edit --feature camel-gson bmtk-base-profile
fabric:profile-edit --feature camel-soap bmtk-base-profile
fabric:profile-edit --feature camel-http bmtk-base-profile
fabric:profile-edit --feature camel-http4 bmtk-base-profile

Dependencias
fabric:profile-edit --bundle wrap:mvn:com.google.android/android/4.1.1.4 bmtk-integrador-profile
fabric:profile-edit --bundle wrap:mvn:org.khronos/opengl-api/gl1.1-android-2.1_r1 bmtk-integrador-profile
fabric:profile-edit --bundle wrap:mvn:io.jsonwebtoken/jjwt/0.6.0 bmtk-integrador-profile

Install componente
fabric:profile-edit --bundle --delete  mvn:com.bursa.integrador.service/integrador-bursa-service/1.0.0 bmtk-integrador-profile
fabric:profile-edit --bundle  mvn:com.bursa.integrador.service/integrador-bursa-service/1.0.0 bmtk-integrador-profile

Install Standalone

features:install camel-core
features:install camel-blueprint
features:install camel-cxf
features:install camel-jackson
features:install camel-gson
features:install camel-soap
features:install camel-http
features:install camel-http4

Dependencies

osgi:install -s wrap:mvn:com.google.android/android/4.1.1.4
osgi:install -s wrap:mvn:org.khronos/opengl-api/gl1.1-android-2.1_r1
osgi:install -s wrap:mvn:io.jsonwebtoken/jjwt/0.6.0


To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache Karaf.
You can run the following command from its shell:

    osgi:install -s mvn:com.mycompany/camel-blueprint/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
